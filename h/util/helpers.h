#ifndef HELPERS_H
#define HELPERS_H

template<typename T>
void set_zero(T& t)
{
    memset(&t, 0, sizeof(T));
}

#endif // HELPERS_H
