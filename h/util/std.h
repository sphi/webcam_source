#ifndef STD_H
#define STD_H

#include <string>
#include <sstream>
using std::string;
using std::to_string;

#include <vector>
using std::vector;

#include <memory>
using std::make_shared;
using std::make_unique;
using std::move;
using std::shared_ptr;
using std::unique_ptr;
using std::weak_ptr;

#include <optional>
using std::make_optional;
using std::nullopt;
using std::optional;

#include <functional>
using std::function;

#include <iostream>

#include <cstring>

template<typename T>
std::ostream& operator<<(std::ostream& os, optional<T> const& opt)
{
    if (opt)
        return os << "optional{" << opt.value() << "}";
    else
        return os << "nullopt";
}

#endif // STD_H
