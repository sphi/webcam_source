#ifndef GTK_UI_H
#define GTK_UI_H

#include <memory>
using std::shared_ptr;
using std::unique_ptr;

#include <functional>
using std::function;

namespace Glib
{
template<class T_CppObject>
class RefPtr;
}

namespace Gdk
{
class GLContext;
}

namespace Gtk
{
class Window;
class Widget;
} // namespace Gtk

struct Renderer;
struct GlContext;

void run_on_gtk_thread(function<void()>&& func);
auto make_viewer_window() -> unique_ptr<Gtk::Window>;
auto make_gl_widget(function<unique_ptr<Renderer>(shared_ptr<GlContext> const&)> renderer_builder)
    -> unique_ptr<Gtk::Widget>;
auto make_gtk_gl_context(Glib::RefPtr<Gdk::GLContext>& gdk_context) -> unique_ptr<GlContext>;

#endif // GTK_UI_H
