#ifndef PIXEL_BUFFER_SOURCE_H
#define PIXEL_BUFFER_SOURCE_H

#include "util/std.h"
#include "util/v2.h"
#include "pixel_buffer.h"

struct PixelBufferSource
{
    static auto open_video4linux2_device(string const& device_path, V2u requested_resolution)
        -> unique_ptr<PixelBufferSource>;

    virtual ~PixelBufferSource() = default;

    virtual auto is_open() const -> bool = 0;
    virtual auto get_resolution() const -> V2u = 0;
    virtual auto get_next_buffer() -> unique_ptr<PixelBuffer> = 0;

    virtual void open_stream() = 0;
    virtual void close_stream() = 0;
};

#endif // PIXEL_BUFFER_SOURCE_H
