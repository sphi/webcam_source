#ifndef COLOR_RGB_H
#define COLOR_RGB_H

struct ColorRGB
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

#endif // COLOR_RGB_H
