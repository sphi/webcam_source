#ifndef PIXEL_FORMAT_H
#define PIXEL_FORMAT_H

#include <cstddef>

struct PixelFormat
{
    size_t size;

    static const PixelFormat RGB;
    static const PixelFormat RGBA;
    static const PixelFormat BGR;
    static const PixelFormat BGRA;
};

#endif // PIXEL_FORMAT_H
