#ifndef PIXEL_BUFFER_H
#define PIXEL_BUFFER_H

#include "pixel_format.h"
#include "util/v2.h"

#include <memory>
using std::unique_ptr;

struct PixelBuffer
{
    static auto make_test_pixel_buffer(V2u dimensions) -> unique_ptr<PixelBuffer>;

    PixelBuffer(PixelFormat const* format)
        : format{format}
    {}
    virtual ~PixelBuffer() = default;
    auto virtual get_raw_data() const -> unsigned char* = 0;
    auto virtual get_dimensions() const -> V2u = 0;

    PixelFormat const* const format;
};

#endif // PIXEL_BUFFER_H
