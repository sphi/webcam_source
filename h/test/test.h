#ifndef TEST_H
#define TEST_H

#include "util/std.h"
#include "util/error.h"

bool run_all_tests();

struct SingleTest
{
    SingleTest(string name, function<void()> f);
};

#define TEST(name)                                            \
    void test##name##func();                                  \
    SingleTest test_internal_##name{#name, test##name##func}; \
    void test##name##func()

#endif // TEST_H
