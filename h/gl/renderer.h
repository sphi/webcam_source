#ifndef RENDERER_H
#define RENDERER_H

#include "util/v2.h"
#include "pixel_buffer/color_rgb.h"

#include <memory>
using std::shared_ptr;
using std::unique_ptr;

struct Texture;
struct GlContext;

struct Renderer
{
    static auto make_color_renderer(ColorRGB const& color, shared_ptr<GlContext> const& context)
        -> unique_ptr<Renderer>;
    static auto make_texture_renderer(shared_ptr<Texture> const& texture, shared_ptr<GlContext> const& context)
        -> unique_ptr<Renderer>;

    virtual ~Renderer() = default;

    virtual void render() = 0;
    virtual void resize(V2u new_size) = 0;
};

#endif // RENDERER_H
