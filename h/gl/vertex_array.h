#ifndef VERTEX_ARRAY_H
#define VERTEX_ARRAY_H

#include "util/v2.h"

#include <memory>
using std::unique_ptr;

#include <functional>
using std::function;

struct VertexArray
{
    static auto make_quad_vertex_array() -> unique_ptr<VertexArray>;

    virtual ~VertexArray() = default;

    virtual void bind_then(function<void()> action) = 0;
};

#endif // VERTEX_ARRAY_H
