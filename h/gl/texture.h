#ifndef TEXTURE_H
#define TEXTURE_H

#include "util/v2.h"

#include <memory>
using std::unique_ptr;

#include <functional>
using std::function;

struct PixelBuffer;

struct Texture
{
    static auto make() -> unique_ptr<Texture>;

    virtual ~Texture() = default;

    virtual void load_from(PixelBuffer const& buffer) = 0;
    virtual void bind_then(function<void()> action) = 0;
    virtual auto get_dimensions() const -> V2u = 0;
};

#endif // TEXTURE_H
