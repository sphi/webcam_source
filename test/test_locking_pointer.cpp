#include "test/test.h"
#include "util/lockable_ptr.h"

struct Foo: SupportsLockablePtr
{
    ~Foo()
    {
        invalidate_lockable_ptrs();
    }
};

TEST(can_create_class_supporting_lockable_ptr)
{
    Foo foo;
}

TEST(can_create_lockable_ptr)
{
    Foo foo;
    auto lockable = LockablePtr(&foo);
}

TEST(can_lock_lockable_ptr)
{
    Foo foo;
    auto lockable = LockablePtr(&foo);
    auto ptr = lockable.lock();
    ASSERT(ptr);
}

TEST(locked_ptr_yeilds_correct_value)
{
    Foo foo;
    auto lockable = LockablePtr(&foo);
    auto ptr = lockable.lock();
    ASSERT(ptr);
    ASSERT(ptr.value().get() == &foo);
}

TEST(locked_ptr_yeilds_correct_value_when_multiple_bases)
{
    struct AAA
    {
        int a;
    };

    struct Bar: AAA, SupportsLockablePtr
    {
        ~Bar()
        {
            invalidate_lockable_ptrs();
        }
    };

    Bar bar;
    auto lockable = LockablePtr(&bar);
    auto ptr = lockable.lock();
    ASSERT(ptr);
    ASSERT(ptr.value().get() == &bar);
}
