#include "util/lockable_ptr.h"

#include <functional>
#include <optional>
#include <mutex>
#include <atomic>
#include <iostream>

struct LockablePtrImpl::Data
{
    Data(void* ptr)
        : ptr{ptr}
    {}

    Data(Data&) = delete;
    void operator=(Data&) = delete;

    std::atomic<void*> ptr;
    std::mutex m;
};

auto LockablePtrImpl::lock(Data* data) -> std::optional<void*>
{
    data->m.lock();
    if (data->ptr) {
        // Don't unlock
        return {data->ptr};
    } else {
        data->m.unlock();
        return std::nullopt;
    }
}

void LockablePtrImpl::unlock(Data* data)
{
    data->m.unlock();
}

SupportsLockablePtr::SupportsLockablePtr()
    : lockable_ptr_data{std::make_shared<LockablePtrImpl::Data>(this)}
{}

SupportsLockablePtr::~SupportsLockablePtr()
{
    if (lockable_ptr_data->ptr) {
        std::cerr
            << "SupportsLockablePtr::invalidate_lockable_ptrs() should have been called at the top of the destructor"
            << std::endl;
        lockable_ptr_data->ptr = nullptr;
    }
    std::lock_guard<std::mutex> guard{lockable_ptr_data->m};
}

void SupportsLockablePtr::invalidate_lockable_ptrs()
{
    lockable_ptr_data->ptr = nullptr;
    // Lock and unlock the mutex to make sure no one is holding on to a pointer aquired before it was cleared
    std::lock_guard<std::mutex> guard{lockable_ptr_data->m};
}

auto SupportsLockablePtr::get_null_data() -> std::shared_ptr<LockablePtrImpl::Data>
{
    static SupportsLockablePtr null_obj;
    null_obj.invalidate_lockable_ptrs();
    return null_obj.lockable_ptr_data;
}
