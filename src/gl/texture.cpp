#include "gl/texture.h"
#include "util/std.h"
#include "util/error.h"
#include "pixel_buffer/pixel_buffer.h"

#include <epoxy/gl.h>

namespace
{
struct TextureImpl : Texture
{
    static auto new_texture_id() -> GLuint
    {
        GLuint id = 0;
        glGenTextures(1, &id);
        ASSERT(id);
        return id;
    }

    TextureImpl()
        : id{new_texture_id()}
        , dimensions{}
    {
        bind_then([]() {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        });
    }

    ~TextureImpl()
    {
        glDeleteTextures(1, &id);
    }

    void bind_then(function<void()> action) override
    {
        glBindTexture(GL_TEXTURE_2D, id);
        action();
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void load_from(PixelBuffer const& buffer) override
    {
        dimensions = buffer.get_dimensions();

        bind_then([&]() {
            ASSERT(buffer.format == &PixelFormat::RGB)
            glTexImage2D(GL_TEXTURE_2D,
                         0,
                         GL_RGBA,
                         dimensions.x,
                         dimensions.y,
                         0,
                         GL_RGB,
                         GL_UNSIGNED_BYTE,
                         buffer.get_raw_data());
        });
    }

    auto get_dimensions() const -> V2u override
    {
        return dimensions;
    }

    GLuint const id;
    V2u dimensions;
};

} // namespace

auto Texture::make() -> unique_ptr<Texture>
{
    return std::make_unique<TextureImpl>();
}
