#include "gl/drawable.h"
#include "gl/shader_program.h"
#include "gl/texture.h"
#include "gl/vertex_array.h"
#include "util/std.h"
#include "util/error.h"

#include <epoxy/gl.h>
#include <glm/glm.hpp>

namespace
{
const string vert_code = "#version 330 core\n"
                         "layout (location = 0) in vec2 position; "
                         "layout (location = 1) in vec2 texture_position_in; "
                         "out vec2 texture_position; "
                         "uniform mat4 transform; "
                         "void main() "
                         "{ "
                         "    gl_Position = transform * vec4(position, 0.0f, 1.0f); "
                         "    texture_position = texture_position_in; "
                         "} ";

const string frag_code = "#version 330 core\n"
                         "in vec2 texture_position; "
                         "out vec4 color; "
                         "uniform sampler2D texture_data; "
                         "void main() "
                         "{ "
                         "    color = texture(texture_data, texture_position); "
                         "} ";

auto lazy_get_texture_shader_program() -> shared_ptr<ShaderProgram>
{
    static shared_ptr<ShaderProgram> program{nullptr};
    if (!program)
        program = ShaderProgram::make(vert_code, frag_code);
    return program;
}

struct TextureDrawable : Drawable
{
    TextureDrawable(shared_ptr<Texture> const& texture)
        : texture{texture}
        , vertex_array{VertexArray::make_quad_vertex_array()}
        , shader_program{lazy_get_texture_shader_program()}
    {}

    void draw() override
    {
        shader_program->bind_then([this]() {
            shader_program->uniform_matrix_f4v("transform", get_glm_mat4_value_ptr());
            texture->bind_then([this]() {
                vertex_array->bind_then([]() { glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr); });
            });
        });
    }

    shared_ptr<Texture> texture;
    unique_ptr<VertexArray> vertex_array;
    shared_ptr<ShaderProgram> shader_program;
};

} // namespace

auto Drawable::make_texture_drawable(shared_ptr<Texture> const& texture) -> unique_ptr<Drawable>
{
    return make_unique<TextureDrawable>(texture);
}
