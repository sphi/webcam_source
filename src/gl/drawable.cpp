#include "gl/drawable.h"
#include "util/std.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct Drawable::Impl
{
    Impl()
        : position{}
        , size{1, 1}
        , rotation{0_degrees}
        , matrix_cache{}
    {
        update_matrix();
    }

    inline void update_matrix()
    {
        matrix_cache = glm::scale(glm::mat4{1.0f}, glm::vec3((float)size.x, (float)size.y, 1.0f));
        matrix_cache = glm::rotate(matrix_cache, glm::radians((float)rotation.radians), glm::vec3(0.0f, 0.0f, 1.0f));
        matrix_cache = glm::translate(matrix_cache, glm::vec3((float)position.x, (float)position.y, 0.0f));
    }

    V2d position;
    V2d size;
    Rotation rotation;
    glm::mat4 matrix_cache;
};

Drawable::Drawable()
    : impl{make_unique<Impl>()}
{}

Drawable::~Drawable() = default;

void Drawable::set_position(V2d pos)
{
    impl->position = pos;
    impl->update_matrix();
}

void Drawable::set_size(V2d size)
{
    impl->size = size;
    impl->update_matrix();
}

void Drawable::set_rotation(Rotation rotation)
{
    impl->rotation = rotation;
    impl->update_matrix();
}

void Drawable::set_transform(V2d pos, V2d size, Rotation rotation)
{
    impl->position = pos;
    impl->size = size;
    impl->rotation = rotation;
    impl->update_matrix();
}

auto Drawable::get_position() -> V2d
{
    return impl->position;
}

auto Drawable::get_size() -> V2d
{
    return impl->size;
}

auto Drawable::get_rotation() -> Rotation
{
    return impl->rotation;
}

auto Drawable::get_glm_mat4_value_ptr() -> float*
{
    return glm::value_ptr(impl->matrix_cache);
}
