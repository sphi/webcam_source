#include "gl/renderer.h"
#include "util/std.h"
#include "util/error.h"
#include "gl/gl_context.h"

#include <epoxy/gl.h>

namespace
{
struct ColorRenderer : Renderer
{
    ColorRenderer(ColorRGB const& color, shared_ptr<GlContext> const& context)
        : context{context}
        , color{color}
    {}

    void render() override
    {
        context->make_current_then([this]() {
            glClearColor(color.r / 255.0, color.g / 255.0, color.b / 255.0, 1.0);
            glClear(GL_COLOR_BUFFER_BIT);
            glFlush();
            WARNING("Rendered color");
        });
    }

    void resize(V2u /*new_size*/) override
    {}

    shared_ptr<GlContext> const context;
    ColorRGB const color;
};
} // namespace

auto Renderer::make_color_renderer(ColorRGB const& color, shared_ptr<GlContext> const& context) -> unique_ptr<Renderer>
{
    return make_unique<ColorRenderer>(color, context);
}
