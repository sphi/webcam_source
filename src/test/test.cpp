#include "test/test.h"

#include <unordered_map>
#include <iostream>
#include <vector>

struct SingleTestData
{
    string name;
    std::function<void()> run;
};

struct AllTests
{
    AllTests() = default;
    AllTests(AllTests&) = delete;

    static auto get_instance() -> AllTests&
    {
        static AllTests instance; // static local so it is initialized by the time it's needed
        return instance;
    }

    vector<SingleTestData> tests;
};

bool run_all_tests()
{
    auto const& tests = AllTests::get_instance();
    int passed_count = 0;
    int failed_count = 0;
    std::cout << "Running " << tests.tests.size() << " tests..." << std::endl << std::endl;
    for (auto const& test : tests.tests) {
        std::cout << test.name << ":";
        try {
            test.run();
            passed_count++;
            std::cout << " passed" << std::endl;
        } catch (std::exception const& e) {
            failed_count++;
            std::cout << " failed!" << std::endl;
            std::cerr << " > " << e.what() << std::endl;
        } catch (...) {
            failed_count++;
            std::cout << " failed!" << std::endl;
            std::cerr << " > Unknown exception" << std::endl;
        }
    }
    std::cout << std::endl << passed_count << " passed, " << failed_count << " failed." << std::endl;
    return failed_count == 0;
}

SingleTest::SingleTest(std::string name, std::function<void()> f)
{
    auto& tests = AllTests::get_instance();
    tests.tests.push_back({name, f});
}
