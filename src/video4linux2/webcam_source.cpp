#include "util.h"
#include "v2.h"
#include "color_rgb.h"

#include <unistd.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <signal.h>

const string device_path = "/dev/video2";

static volatile bool should_quit = false;

void sigint_handler(int)
{
    std::cout << "Caught SIGINT, stopping..." << std::endl;
    should_quit = true;
}

int loopback_main(int argc, char** argv)
{
    signal(SIGINT, sigint_handler);

    auto buffer_dim = V2i(1080, 720);
    int pixel_count = buffer_dim.x * buffer_dim.y;
    ASSERT(sizeof(ColorRGB) == 3);
    int buffer_data_size = pixel_count * sizeof(ColorRGB);

    int sink_fd = open(device_path.c_str(), O_WRONLY);
    if (sink_fd < 0) {
        std::cout << "Failed to open " << device_path << ", make sure "
                  << "v4l2loopback (https://github.com/umlaeute/v4l2loopback) "
                  << "is installed and loaded" << std::endl;
        exit(1);
    }
    std::cout << "Successfully opened " << device_path << std::endl;

    struct v4l2_format format;
    format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    ASSERT(ioctl(sink_fd, VIDIOC_G_FMT, &format) >= 0);
    format.fmt.pix.width = buffer_dim.x;
    format.fmt.pix.height = buffer_dim.y;
    format.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
    format.fmt.pix.sizeimage = buffer_data_size;
    ASSERT(ioctl(sink_fd, VIDIOC_S_FMT, &format) >= 0);
    std::cout << "Successfully did the ioctl shit" << std::endl;

    ColorRGB* buffer = new ColorRGB[pixel_count];

    std::cout << "Starting render loop" << std::endl;
    int j = 0;
    while (!should_quit) {
        // cout << "grabbing frame" << endl;

        for (int i = 0; i < pixel_count; i++) {
            buffer[i].r = (unsigned char)(((double)((i + j) % pixel_count) / pixel_count) * 255.0);
            buffer[i].g = 128;
            buffer[i].b = ((i / 20) % 2) ? 0 : 255;
        }

        ASSERT(buffer_data_size == write(sink_fd, buffer, buffer_data_size));

        // cout << "writing frame" << endl;
        j = (j + 1000) % pixel_count;
        usleep(33000);
    }
    std::cout << "Exited render loop" << std::endl;

    close(sink_fd);

    std::cout << "Done" << std::endl;
    return 0;
}
