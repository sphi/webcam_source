#include "util/error.h"
#include "util/std.h"
#include "gl/renderer.h"
#include "gtk_ui/gtk_ui.h"
#include "gl/gl_context.h"

#include <gtkmm/glarea.h>

struct GLWidgert : Gtk::GLArea
{
    GLWidgert(function<unique_ptr<Renderer>(shared_ptr<GlContext> const&)> renderer_builder)
        : renderer_builder{renderer_builder}
    {
        set_hexpand(true);
        set_vexpand(true);
        set_auto_render(true);
        /*set_has_alpha(false);
        set_has_depth_buffer(false);*/
    }

    /*
    void try_gl(std::string const& context, function<void()> work)
    {
        auto gl_error_to_string = [](Gdk::GLError const& error) -> auto {
            return error.what() + " (" + error.domain() + " :: " + to_string(error.code()) + ")";
        };
        try {
            GLArea::throw_if_error();
        } catch (Gdk::GLError const& error) {
            WARNING("OpenGL error before " << context << ": " << gl_error_to_string(error));
        }
        try {
            work();
            GLArea::throw_if_error();
        } catch (Gdk::GLError const& error) {
            WARNING("OpenGL error during " << context << ": " << gl_error_to_string(error));
        } catch (std::runtime_error const& error) {
            WARNING(context << ":\n    " << error.what());
        }
    }
    */

    void on_realize() override
    {
        GLArea::on_realize();
        GLArea::make_current();
        try {
            auto gdk_context = get_context();
            GLArea::throw_if_error();
            ASSERT(gdk_context);
            shared_ptr<GlContext> context = make_gtk_gl_context(gdk_context);
            renderer = renderer_builder(context);
        } catch (Gdk::GLError const& error) {
            WARNING("OpenGL error realizing widget: " << error.what());
        } catch (std::runtime_error const& error) {
            WARNING("Error realizing OpenGL widget:\n    " << error.what());
        }
    }

    void on_unrealize() override
    {
        GLArea::make_current();
        renderer = nullopt;
        GLArea::on_unrealize();
    }

    auto on_render(const Glib::RefPtr<Gdk::GLContext>& context) -> bool override
    {
        try {
            throw_if_error();
            ASSERT(renderer);
            renderer.value()->render();
        } catch (std::runtime_error const& error) {
            WARNING("Error rendering:\n    " << error.what());
        } catch (Gdk::GLError const& error) {
            WARNING("OpenGL error in render: " << error.what());
        }
        GLArea::on_render(context);
        return true;
    }

    void on_resize(int width, int height) override
    {
        GLArea::on_resize(width, height);
        if (renderer) {
            ASSERT(width > 0);
            ASSERT(height > 0);
            renderer.value()->resize(V2u{(unsigned)width, (unsigned)height});
        }
    }

    optional<unique_ptr<Renderer>> renderer;
    function<unique_ptr<Renderer>(shared_ptr<GlContext> const&)> renderer_builder;
};

auto make_gl_widget(function<unique_ptr<Renderer>(shared_ptr<GlContext> const&)> renderer_builder)
    -> unique_ptr<Gtk::Widget>
{
    return make_unique<GLWidgert>(renderer_builder);
}
