#include "util/std.h"
#include "util/error.h"
#include "util/lockable_ptr.h"
#include "gtk_ui/gtk_ui.h"
#include "gl/renderer.h"
#include "gl/texture.h"
#include "pixel_buffer/pixel_buffer.h"
#include "pixel_buffer/pixel_buffer_source.h"

#include <gtkmm/applicationwindow.h>
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <thread>

auto const device_path = "/dev/video0";

auto make_custom_widget() -> unique_ptr<Gtk::Widget>;

struct CaptureThread : SupportsLockablePtr
{
    CaptureThread(unique_ptr<PixelBufferSource> source, function<void(unique_ptr<PixelBuffer>)> on_new_buffer)
        : source{move(source)}
        , on_new_buffer{on_new_buffer}
    {
        std::thread{std::bind(&CaptureThread::capture_loop, LockablePtr{this})}.detach();
    }

    ~CaptureThread()
    {
        invalidate_lockable_ptrs();
    }

    static void capture_loop(LockablePtr<CaptureThread> lockable)
    {
        {
            auto self = lockable.lock();
            if (self)
                self.value()->source->open_stream();
        }
        while (true) {
            {
                auto self = lockable.lock();
                if (self) {
                    auto buffer = self.value()->source->get_next_buffer();
                    self.value()->on_new_buffer(move(buffer));
                } else {
                    break;
                }
            }
            usleep(1);
        }
    }

    unique_ptr<PixelBufferSource> const source;
    function<void(unique_ptr<PixelBuffer>)> const on_new_buffer;
};

struct ViewerWindow
    : Gtk::ApplicationWindow
    , SupportsLockablePtr
{
    ViewerWindow()
        : box(Gtk::ORIENTATION_VERTICAL)
        , button{"Refresh"}
        , advance_button{"Advance"}
        , quit_button{"Quit"}
        , preview_texture{}
        , preview_widget{make_gl_widget([this](shared_ptr<GlContext> const& context) -> auto {
            return Renderer::make_texture_renderer(lazy_get_preview_texture(), context);
            // return Renderer::make_color_renderer(ColorRGB{0, 128, 255}, context);
        })}
        , capture_thread{make_unique<CaptureThread>(
              PixelBufferSource::open_video4linux2_device(device_path, {1280, 720}),
              std::bind(&ViewerWindow::update_buffer_on_capture_thread, this, std::placeholders::_1))}
    {
        set_title("Viewer");
        set_default_size(800, 600);
        add(box);
        box.pack_start(*preview_widget, Gtk::PACK_EXPAND_WIDGET);
        box.pack_start(button, Gtk::PACK_SHRINK);
        box.pack_start(advance_button, Gtk::PACK_SHRINK);
        box.pack_start(quit_button, Gtk::PACK_SHRINK);
        advance_button.signal_clicked().connect(sigc::mem_fun(this, &ViewerWindow::on_button_advance));
        button.signal_clicked().connect(sigc::mem_fun(this, &ViewerWindow::on_button_pressed));
        quit_button.signal_clicked().connect(sigc::mem_fun(this, &ViewerWindow::on_quit_pressed));
        preview_widget->show();
        show_all_children();
    }

    ~ViewerWindow()
    {
        invalidate_lockable_ptrs();
    }

    void update_buffer_on_capture_thread(shared_ptr<PixelBuffer> buffer)
    {
        run_on_gtk_thread([lockable = LockablePtr{this}, buffer]() {
            auto self = lockable.lock();
            if (self) {
                self.value()->lazy_get_preview_texture()->load_from(*buffer);
                self.value()->preview_widget->queue_draw();
            }
        });
    }

    auto lazy_get_preview_texture() -> shared_ptr<Texture>
    {
        if (!preview_texture) {
            preview_texture = Texture::make();
        }
        return preview_texture.value();
    }

    void on_button_pressed()
    {}

    void on_quit_pressed()
    {
        preview_widget->hide();
        close();
    }

    void on_button_advance()
    {}

    Gtk::Box box;
    Gtk::Button button;
    Gtk::Button advance_button;
    unsigned size{8};
    Gtk::Button quit_button;
    optional<shared_ptr<Texture>> preview_texture;
    unique_ptr<Gtk::Widget> preview_widget;
    unique_ptr<CaptureThread> capture_thread;
    // optional<unique_ptr<PixelBuffer>> pending_buffer;
};

auto make_viewer_window() -> std::unique_ptr<Gtk::Window>
{
    return make_unique<ViewerWindow>();
}
