#include "gl/gl_context.h"
#include <util/std.h>
#include <util/error.h>

#include <gdkmm/glcontext.h>

struct GtkGlContext : GlContext
{
    GtkGlContext(Glib::RefPtr<Gdk::GLContext>& gdk_context)
        : gdk_context{gdk_context}
    {}

    void make_current_then(function<void()> action) override
    {
        bool started_current{Gdk::GLContext::get_current() == gdk_context};
        if (!started_current)
            gdk_context->make_current();
        action();
        if (Gdk::GLContext::get_current() != gdk_context)
            WARNING("GL contexted changed during GtkGlContext::make_current_then()");
        if (!started_current)
            Gdk::GLContext::clear_current();
    }

    Glib::RefPtr<Gdk::GLContext> gdk_context;
};

auto make_gtk_gl_context(Glib::RefPtr<Gdk::GLContext>& gdk_context) -> unique_ptr<GlContext>
{
    return make_unique<GtkGlContext>(gdk_context);
}
